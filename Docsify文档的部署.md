?> 终于写完一个Docsify文档了，不把它放到网上供人类观赏怎么行！！ :volcano:

## 1. 部署到服务器站点上
在站点访问的根目录下，直接把docs整个目录上传即可。(或者直接使用git管理该文档，ssh进入到站点目录后直接`git clone xxx`即可)

比如说 站点`https://shafish.cn`在服务器上的根路径为：`/www/website/shafish.cn`，

此时直接在`/www/website/shafish.cn`目录中执行`git clone https://gitee.com/shafish/docsify-reference` 就完成了该文档的部署，简单得yipi

最后该文档的访问路径就是`https://shafish.cn/docsify-reference`，就是这么简单！:ghost:

## 2. 部署到Github上
参考：https://docsify.js.org/#/deploy?id=deploy
看着不难，这里就不折腾了 :trollface:

## 3. 我就下载到本地看看
- 直接`git clone https://gitee.com/shafish/docsify-reference`下载到本地合适目录
- `cd  docsify-reference`
- 用浏览器打开`index.html`即可。:yum::yum: