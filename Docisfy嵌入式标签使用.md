?> 在内容中的加入嵌入式标签:video, audio, iframes, code， markdown 显示对应内容

## 1. 使用嵌入标签的格式

`[filename](filepath/name.xx ':include')`  

- xx资源必须为`html` `.htm` `.markdown` `.md` `.mp3` `.mp4` `.ogg`时使用`:include`才能自动识别。
- 使用时注意需要上下都空一行。

## 2. 嵌入markdown格式内容

- 在当前根目录下创建一个`_embed`目录，在目录中创建`markdown.md`文件，内容如下：
```md
嵌入一个markdown格式的markdown文件：你好吖 :trollface:
```

- 在适当的地方引入：`[embed_markdown](_embed/markdown.md ':include')` 即可

- 效果：

[embed_markdown](_embed/markdown.md ':include')


## 3. 嵌入iframes格式内容
- 格式：
`[shafish](https://hagfish.cn ':include :type=iframe width=100% height=400px')`
  - :type 指定嵌入资源需要放在一个iframe中，加载网页常用。
  - width/height 指定了iframe的长度。

- 效果：

[shafish](https://shafish.cn ':include :type=iframe width=100% height=400px')

## 4. 嵌入video格式内容
- 格式：
`[四月](https://down.shafish.cn/7.mp4 ':include')`

- 效果：

[四月](https://cdn.shafish.cn/video/mp4/7.mp4 ':include')

## 5. 嵌入code格式内容
- 生成code
在当前根目录下创建一个`_embed`目录，在目录中创建`markdown.md`文件，内容如下：
```js
const result = fetch(`${URL}:${PORT}`)
  .then(function (response) {
    return response.json()
  })
  .then(function (myJson) {
    console.log(JSON.stringify(myJson))
  })
```

- 格式：
`[js](_embed/code.js ':include')`

- 效果：

[js](_embed/code.js ':include')