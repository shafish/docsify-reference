?> docsify默认提供的主题是头文件中的：`<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/docsify/lib/themes/vue.css">`，更换主题时只需要替换该文件即可。

## 1. Docsify中可用的主题
```js
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/docsify/themes/vue.css">
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/docsify/themes/buble.css">
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/docsify/themes/dark.css">
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/docsify/themes/pure.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/docsify-themeable@0/dist/css/theme-defaults.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/docsify-themeable@0/dist/css/theme-simple.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/docsify-themeable@0/dist/css/theme-simple-dark.css">
```

本文档使用的就是[docsify-themeable](https://jhildenbiddle.github.io/docsify-themeable/#/quick-start)提供的`theme-simple-dark.css`主题，效果如下：

![](https://cdn.shafish.cn/imgs/Docsify/docs/DeepinScreenshot_select-area_20201018180118.png)

> 具体主题设置参数参考：https://docsify.js.org/#/themes