?> 其实我们初始化Docsify文档默认已经加载了侧边栏功能，当然我们本篇介绍侧边栏就是要自定义它，先来看看效果：

![](https://cdn.shafish.cn/imgs/Docsify/docs/DeepinScreenshot_select-area_20201016000235.png)

## 1. 侧边栏配置说明
首先你需要知道侧边栏对应的markdown文件名为：`_sidebar.md`，位于根目录下;

然后还需要在`index.html`配置中开启docsify的侧边栏：`loadSidebar: true`。默认效果如下：

![](https://cdn.shafish.cn/imgs/Docsify/docs/DeepinScreenshot_select-area_20201015230834.png "显示 `Docify文档使用教程` 是设置了name值为`Docify文档使用教程`的原因")

此时开启了侧边栏的配置文件`index.html`内容为：
```js
<script>
  window.$docsify = {
    name: 'Docify文档使用教程',
    loadSidebar: true
  }
</script>
```

## 2. 修改_sidebar.md文件
?> 需求：侧边栏添加条目(也就是一级标题)Test，点击该条目则在右边显示test.md中的内容。

- 下面来一起试试吧：
上面说到的`_sidebar.md`文件指定了侧边栏的内容，那我们就在docs根目录下创建该`_sidebar.md`文件，再把下面的代码编写到该文件中看看：

```md
* [Home](/)
* [Test](test.md)
```
`_sidebar.md`文件添加了两个关键字链接 Home和Test，分别对应当前根目录和根目录下的test.md文件，下面直接来看看效果：

![](https://cdn.shafish.cn/imgs/Docsify/docs/DeepinScreenshot_select-area_20201015232303.png)


ok，发现使用`[条目名称](条目内容)`这样的格式，Docsify就会自动把条目名称加入到侧边栏并把内容输出到右边。

现在你已经掌握如何给docsify文档添加需要的侧边栏条目lie，再自行创建一个名为`第一章`的条目放到侧边栏来试着练练手吧！！:clap: :clap:

## 3. 进一步修改test.md文件
?> 要求在侧边栏显示`test.md`文件中的二级标题、三级标题和四级标题（一级标题当然就是条目本身la）

先看看效果：
![](https://cdn.shafish.cn/imgs/Docsify/docs/DeepinScreenshot_select-area_20201015235257.png)

?> 点击`Test`条目还会折叠/展开哟。

想要实现这种效果，除了修改对应的`test.md`文件<sup>[3.2](#_32-testmd文件内容)</sup> 外，还需要在`index.html`配置文件<sup>[3.1](#_31-侧边栏对应参数设置)</sup> 中指定侧边栏显示的层级`subMaxLevel:4`。（其中4表示标题的层级数，这里写4表示可以显示包括条目本身在内一共4层标题）

避免层级过深，造成侧边栏内容被遮盖，在实际文档中我们只需要显示2到3层即可。效果如下：

![](https://cdn.shafish.cn/imgs/Docsify/docs/DeepinScreenshot_select-area_20201016000109.png)

![](https://cdn.shafish.cn/imgs/Docsify/docs/DeepinScreenshot_select-area_20201016000235.png)

### 3.1 侧边栏对应参数设置
```js
<script>
  window.$docsify = {
    name: 'Docify文档使用教程',
    loadSidebar: true,
    subMaxLevel:4
  }
</script>
```

### 3.2 test.md文件内容
```md
只是一个test markdown文件

## 二级标题

## 二级标题

### 三层标题

### 三层标题

### 三层标题

## 二级标题
```

## 4. 隐藏内容标题，不显示在侧边栏中
?> 以上面的`test.md`文件为例子，我们把最后多出来的那个二级标题隐藏掉，让它不出现在侧边栏中

效果实现也很简单，在需要被隐藏的标题<sup>[4.1](#_41-testmd文件内容)</sup>后添加`<!-- {docsify-ignore} -->`即可。

![](https://cdn.shafish.cn/imgs/Docsify/docs/DeepinScreenshot_select-area_20201016001158.png)

![](https://cdn.shafish.cn/imgs/Docsify/docs/DeepinScreenshot_select-area_20201016001306.png)

### 4.1 test.md文件内容
```md
只是一个test markdown文件

## 二级标题

## 二级标题

### 三层标题

### 三层标题

### 三层标题

## 二级标题 <!-- {docsify-ignore} -->
```

OK!!到这里关于docsify侧边栏的功能你已经完全掌握啦，撒花，继续努力坚持 ！*★,°*:.☆(￣▽￣)/$:*.°★* 。