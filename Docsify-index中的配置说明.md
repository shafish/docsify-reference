?> 这里的配置项是指`index.html` `<script>xx</script>`js脚本中xx设置的内容。

## 1. 已经使用的配置参数
下面输出一下我们前面配置过的所有信息：
```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Document</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="description" content="Description">
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/docsify/lib/themes/vue.css">
</head>
<body>
  <div id="app">那么猴急干嘛，人家还没准备好呢</div>  <!--内容加载完成前，浏览器页面显示的内容-->
  <script>
    window.$docsify = {      
      name: 'Docify文档使用教程',
      loadSidebar: true,
      subMaxLevel: 4,
      loadNavbar: true,
      coverpage: true
    }
  </script>
  <script src="//cdn.jsdelivr.net/npm/docsify/lib/docsify.min.js"></script> <!--docsify模块-->
  <script src="//cdn.jsdelivr.net/npm/docsify/lib/plugins/emoji.min.js"></script> <!--docsify emoji组件-->
</body>
</html>
```

## 2.参数介绍
下面慢慢来一个个介绍：

- 1.`<div id="app">内容</div>`：body标签就这一个容器可以装显示内容，毫无疑问，我们编写的markdown文件最终要加载到这个div标签体中。
- 2.`id`：默认如果值为app，docsify会自动绑定$docsify对象，但是我们改为`id="main"` `id="shafish"` `id="帅比"` 行不行？使用`el`跟`$docsify对象`绑定即可。这也是vue的入门用法。
- 3.`el`:把上面`index.html`文件body标签改为以下内容：（只改了绑定的id值，最终显示的效果也是一毛一样的）
 ```html
 <body>
  <div id="帅比">那么猴急干嘛，人家还没准备好呢</div>
  <script>
    window.$docsify = {      
      el: '#帅比',
      name: 'Docify文档使用教程',
      loadSidebar: true,
      subMaxLevel: 4,
      loadNavbar: true,
      coverpage: true,
      auto2top: true,
    }
  </script>
  <script src="//cdn.jsdelivr.net/npm/docsify/lib/docsify.min.js"></script>
  <script src="//cdn.jsdelivr.net/npm/docsify/lib/plugins/emoji.min.js"></script>
</body>
</html>
 ```
- 4.`name`：docsify文档的文档主标题（文档最左上方显示的标题）
- 5.`loadSidebar`：开启侧边栏
- 6.`subMaxLevel`：在侧边栏中显示多级标题（使用数字表示显示多少层级标题）
- 7.`loadNavbar`：开启导航栏
- 8.`coverpage`：开启欢迎页
- 9.`externalLinkTarget`：markdown文件中链接`[关键字](链接)`的跳转方式：_blank
- 10.`requestHeaders`:请求头内容 'x-token': 'xxx'这些
- 11.`notFoundPage`：设置404内容，对应`_404.md`文件
- 12.`auto2top`:文件的访问路径修改后自动跳转到文章顶部
- 13.`homepage`：文件夹的默认显示内容，如果不设置该值，默认显示`README.md`中的内容
- 14.`basePath`:设置文件的访问域名，可以指定文件的访问路径，比如你的docsify文件存放在`https://shafish.cn/docs`目录下，该目录下包含README.md、test.md等等文件，你就可以直接设置basepath为：`https://shafish.cn/docs`，否则默认为当前根目录。（该配置可提供动态加载别的目录下文件的访问）
- 15.`relativePath`:开启相对路径，为`true`后可以在路由中使用`..`表示上层目录，`.`表示当前目录
- 16.`logo`:可以在侧边栏设置一个文档的图标
- 17.`themeColor: '#3F51B5'`:可以设置主题的颜色
- 18.`alias`：可以设置路由的别名，在本地访问路径过长或者文件名过长的情况下设置一个唯一值。
- 19.`autoHeander`：为true可以在右边内容的顶部显示该条目名（也就是之前侧边栏提到的一级标题）作为开头。
- 20.`mergeNavbar`：在小屏下侧边栏和导航栏到合并在一起显示
- 21.`formatUpdated`说可以显示文件的修改时间（没看到显示在哪）
- 22.`noCompileLinks: ['/foo', '/bar/.*'],`：表示/foo和/bar文件夹下的所有文件中的链接不经过docsify hash等处理 https://github.com/docsifyjs/docsify/issues/203
- 23.`topMargin: 0`，0代表几乎贴到浏览器书签栏，可以设置为10等间隔一下
![](https://cdn.shafish.cn/imgs/Docsify/docs/DeepinScreenshot_select-area_20201019234215.png)

提供的参数可能有点多，但肯定不是每个参数都需要用上，只需大致了解用法即可。也可以下载[本文档的gitee仓库](https://gitee.com/shafish/docsify-reference)参考如何使用。

> 更多参数：https://docsify.js.org/#/configuration