?> 来看看效果吧：在右上角添加了翻译按钮，可以显示对应语言的文件。

![](https://cdn.shafish.cn/imgs/Docsify/docs/DeepinScreenshot_select-area_20201016231416.png)

## 1. 导航拦参数说明
跟侧边栏需要编写`_sidebar.md`相似，添加导航栏也同样需要在根目录下编写一个叫`_navbar.md`的markdown文件，然后在配置参数`loadNavbar: true`。

开启导航栏后，`index.html`中的参数为：
```js
<script>
  window.$docsify = {
    name: 'Docify文档使用教程',
    loadSidebar: true,
    subMaxLevel:4,
    loadNavbar: true
  }
</script>
```

## 2. 修改_navbar.md文件
在docs根目录下创建一个`_navbar.md`文件，编写以下内容

```md
* [chinese](/)
* [english](/en/)
```

`/`表示当前docs根目录为默认中文显示

`/en/`表示当前docs根目录下的en文件夹存放对应英文的文档，这个文件夹我们之前还没有创建

现在创建`docs/en/`目录后，再创建一个首页文件`README.md`，在其中写入`hi,this is an english documen.`，效果如下：

![](https://cdn.shafish.cn/imgs/Docsify/docs/DeepinScreenshot_select-area_20201016224722.png)

![](https://cdn.shafish.cn/imgs/Docsify/docs/DeepinScreenshot_select-area_20201016224827.png)
![](https://cdn.shafish.cn/imgs/Docsify/docs/DeepinScreenshot_select-area_20201016224845.png "选择english，则显示en目录下的首页内容")

可以看到点击chinese显示的是根目录下的内容，而点击english则显示根目录下`en`文件夹的内容。

### 2.1 使用国旗来表示国家
用文字表示国家-表示有点单调，我们下面来使用docsify的`emoji`插件来获取国家的国旗。

- 在`index.html`配置文件中引入emoji插件就可以直接在markdown文件中直接使用了，很方便：
```js
<script>
    window.$docsify = {      
    name: 'Docify文档使用教程',
    loadSidebar: true,
    subMaxLevel: 4,
    loadNavbar: true
    }
</script>
<script src="//cdn.jsdelivr.net/npm/docsify/lib/docsify.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/docsify/lib/plugins/emoji.min.js"></script>
```
引入了：`<script src="//cdn.jsdelivr.net/npm/docsify/lib/plugins/emoji.min.js"></script>`

- 在文件`_navbar.md`中使用

```md
* translate

* [:cn:](/)
* [:us:, :uk:](/en/)
```