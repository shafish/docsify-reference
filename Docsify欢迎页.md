?> 先来看看效果：

![](https://cdn.shafish.cn/imgs/Docsify/docs/DeepinScreenshot_select-area_20201017000657.png)

## 1. 导航拦参数说明
经过上面`_sidebar` `_navbar`这个两个功能设置，相信你已经熟悉docsify添加页面的套路了：`在配置文件中开启对应开关`+`创建对应文件`。

毫无疑问，添加docsify欢迎页的套路也是一毛一样的。参数为：`coverpage: true` 文件名为：`_coverpage.md`

开启欢迎页后，`index.html`中的参数为：
```js
<script>
  window.$docsify = {
    name: 'Docify文档使用教程',
    loadSidebar: true,
    subMaxLevel:4,
    loadNavbar: true,
    coverpage: true
  }
</script>
```

## 2. 修改_coverpage.md文件
在docs根目录下创建一个`_coverpage.md`文件，编写以下内容

```md
# docsify文档编写计划 <small>1.0</small>

> 只是官网的一个翻译、一个实操的翻译.

- 一个木得感情的翻译文档
- 一个niupi的翻译文档
- 一个没有废话的翻译文档（因为通篇都是废话）

[GitHub](https://gitee.com/shafish/ToolMan/blob/master/Docsify%E4%BD%BF%E7%94%A8%E6%96%87%E6%A1%A3.md)
[Get Started](#Headline)
```

来看看效果：
![](https://cdn.shafish.cn/imgs/Docsify/docs/DeepinScreenshot_select-area_20201017002136.png)

有点意思，在该文件中定义的内容都默认设置为居中显示：
- 使用`#`显示文档的大标题
- 使用`-`和`>` 都是基本的文本提示
- 而使用`[关键字](链接)`关键字，显示的则是一个跳转按钮，这个需要特别注意

[Get Started](#Headline)跳转的`#Headline`是根目录中首页文件`README.md`的一级标题，点击按钮就直接跳转到内容首页。

官方文档还有关于各目录下的`_coverpage.md`设置，需要设不同目录滴盆友请看[官方文档](https://docsify.js.org/#/cover)适当自定调整。

欢迎页的设置就这么简单的完成了！！撒花！*★,°*:.☆\(￣▽￣)/$:*.°★* 

