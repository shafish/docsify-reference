?> Docsify插件列表：https://docsify.js.org/#/awesome?id=plugins

- 1. 全文搜索
```js
<script>
    window.$docsify = {
      xxx
      search: {
        depth: 4,
        noData: 'No results!',
        placeholder: 'Search...'
      },
      xxx
    }
</script>
<script src="//cdn.jsdelivr.net/npm/docsify/lib/plugins/search.min.js"></script>
```

- 2. 图片缩小放大
```js
<script src="//cdn.jsdelivr.net/npm/docsify/lib/plugins/zoom-image.min.js"></script>
```
使用`![](image.png ":no-zoom")`设置当前图片不设置点击放大

- 3. 代码点击复制
```js
<script src="//cdn.jsdelivr.net/npm/docsify-copy-code"></script>
```

- 4. 上一页/下一页
```js
<script src="//cdn.jsdelivr.net/npm/docsify-pagination/dist/docsify-pagination.min.js"></script>
```

- 5. 评论插件
  - [gittalk](https://docsify.js.org/#/plugins?id=gitalk)：利用github issue功能提供的一个简洁高效的评论系统，需要使用github帐号登录后才能评论。
  - [valine](https://www.npmjs.com/package/docsify-valine)：结合了leadcloud的一个serverless评论系统，需要在leadcloud注册一个应用获取key与id，简单设置即可，只需要提供用户名+邮箱就可以评论，不强制登录。

> 更多插件介绍：https://docsify.js.org/#/plugins

ps：选一个合适的主题直接使用，再选择适合的插件适当调整即可（把更多的精力放在文档质量上）。