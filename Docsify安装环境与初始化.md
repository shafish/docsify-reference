## 1. Linxu/Mac下安装NodeJS
- 推荐使用nvm安装：
    - 1.1去`https://github.com/nvm-sh/nvm#install--update-script`下安装最新版本nvm：
    ```bash
    # 注意访问raw需要挂代理
    curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.36.0/install.sh | bash
    ```
    - 1.2.添加nvm命令到环境中:（在.zshrc或者.bashrc末尾直接添加）
    ```bash
    export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \\. "$NVM_DIR/nvm.sh" # This loads nvm
    ```
    - 1.3.终端输入`nvm --version`命令，正确输出版本信息即说明安装成功:
    ```bash
    nvm --version
    ```
    - 1.4.选择nodejs的稳定版本下载`nvm ls-remote`，这里选择最新的稳定版本`v12.19.0`:
    ```bash
    nvm ls-remote  # 查看所有NodeJS版本
    nvm install v12.19.0
    ```
    - 1.5.OK！！完成NodeJS环境的安装

## 2. 安装docsify-cli模块
```bash
npm i docsify-cli -g
```

## 3. 初始化一个Docsify文档
?> 不用在当前目录创建docs目录，直接执行命令即可

```bash
docsify init ./docs
```

## 4. 运行你的第一个docsify项目
```bash
cd docs && docsify serve
```

默认访问`http://localhost:3000/#/` 看看下面的效果，ok完美！！！
![](https://cdn.shafish.cn/imgs/Docsify/docs/DeepinScreenshot_select-area_20201015225929.png)